#Joomla FirePHP Plugin

###本插件依赖FireFox浏览器、Firebug扩展和FirePHP扩展

###使用方法
1. 下载本插件zip包，使用Joomla扩展安装，然后启用即可在Joomla项目全局内使用firephp调试语句（一般情况下）。
2. 特殊情况下，可能firephp插件还没有加载就调用firephp调试语句，则会出现错误，此时需要在调试语句前面手动加载：
	`if(!class_exists('FirePHP')) require JPATH_ROOT.'/plugins/system/firephp/FirePHPCore/fb.php';`
3. 常用firephp调试语句有：
	```
		(1) fb($var);						//打印变量
		(2) fb($var,FirePHP::TRACE);		//跟踪变量来源，也可以用fb(1,FirePHP::TRACE);仅记忆跟踪位置标号
	```
4. 在需要跟踪的地方加入上面两个语句即可。


###FirePHP官方文档
http://www.firephp.org/HQ/Use.htm

